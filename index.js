const { Client, Intents } = require("discord.js");
const dotenv = require("dotenv");
const PREFIX = "$";

dotenv.config();

const client = new Client({
  intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES],
});

// const random_number = () => {
//   return Math.floor(Math.random() * 10);
// };

client.on("ready", () => {
  console.log("you bot is ready");
  console.log(`${client.user.tag} has logged in.`);
  client.user.setActivity("anime", { type: "WATCHING" });
});

client.on("messageCreate", (message) => {
  if (message.author.bot) return;
  if (message.content.startsWith(PREFIX)) {
    const [CMD_NAME, ...args] = message.content
      .trim()
      .substring(PREFIX.length)
      .split(/\s+/);
    // console.log(CMD_NAME);
    // console.log(args);
    if (CMD_NAME === "kick") {
      if (args.length === 0) return message.reply("Please provide an ID");
      const member = message.guild.members.cache.get(args[0]);
      if (member) {
        member
          .kick()
          .then((member) => message.channel.send(`${member} was kicked.`))
          .catch((err) => message.channel.send("I cannot kick that user :("));
      } else {
        message.channel.send("That member was not found");
      }
    }
  }
});

client.on("messageCreate", (message) => {
  if (message.author.bot) return;
  console.log(`[${message.author.tag}]: ${message.content} `);
  if (message.content === "hello") {
    message.channel.send("hello");
    message.reply("ไง!? เหงาอะดิ");
  }
});

// client.on("messageCreate", (msg) => {
//   if (msg.author.bot) return;
//   let foods = [
//     "ข้าวไก่ทอด",
//     "ข้าวมันไก่",
//     "ข้าวไข่เจียว",
//     "ข้าวขาหมู",
//     "ข้าวลาบน้ำตก",
//     "ข้าวแกงเขียวหวาน",
//     "ข้าวไข่ต้ม",
//     "ข้าวไข่ดาว",
//     "ข้าวต้ม",
//     "ข้าวเปล่าเฉยๆเนี่ยแหละ",
//   ];
// //   if (msg.author.bot) return;
//   if (msg.content == "หิว") {
//     msg.reply(foods[random_number()]);
//   }
//   if (msg.content == "อย่าโง่") {
//     msg.reply("https://www.google.com/?safe=active&ssui=on");
//     msg.reply("หาความรู้ซะไอโง่");
//   }
// });

client.login(process.env.TOKEN);
